package com.khanas;

import com.khanas.view.MyView;

import java.util.concurrent.ExecutionException;


public class Application {

    public static void main(final String[] args) throws ExecutionException, InterruptedException {
        new MyView();
    }
}
