package com.khanas.part1.model;


import java.util.concurrent.Callable;

public class FibonaciCallable implements Callable<Integer> {

    private int n;

    public FibonaciCallable(final int n) {
        this.n = n;
    }

    public final Integer call() {
        int sum = 1;
        long f0 = 0;
        long f1 = 1;
        for (int i = 0; i < n; i++) {
            long fn = f0 + f1;
            f0 = f1;
            f1 = fn;
            sum += fn;
        }
        return sum;
    }
}
