package com.khanas.part1.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.util.Random;


public class Producer extends Thread {
    Logger logger = LogManager.getLogger();
    private DataOutputStream out;
    private Random rand = new Random();

    public Producer(OutputStream os) {
        out = new DataOutputStream(os);
    }

    public final void run() {
        for (int i = 0; i < 10; i++) {
            try {
                double num = rand.nextDouble();
                logger.info("Producer: " + num);
                out.writeDouble(num);
                out.flush();
                sleep(Math.abs(rand.nextInt() % 1000));
            } catch (Exception e) {
                logger.error("Error: " + e);
            }
        }
    }


}
