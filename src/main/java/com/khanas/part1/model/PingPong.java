package com.khanas.part1.model;

public class PingPong {
    private volatile static long a = 0;
    private static Object sync = new Object();

    public PingPong() {
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
        }
        System.out.println("Count expected(20000).Get = " + a);
    }

    private Thread thread1 = new Thread(() -> {
        synchronized (sync) {
            for (int i = 1; i <= 10000; i++) {
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                }
                a++;
                sync.notify();
            }
        }
    });
    private Thread thread2 = new Thread(() -> {
        synchronized (sync) {
            for (int i = 1; i <= 10000; i++) {
                sync.notify();
                try {
                    sync.wait();
                } catch (InterruptedException e) {
                }
                a++;
            }
        }
    });


}
