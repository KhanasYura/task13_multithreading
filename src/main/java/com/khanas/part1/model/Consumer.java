package com.khanas.part1.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Consumer extends Thread {
    Logger logger = LogManager.getLogger();
    private DataInputStream in;

    public Consumer(InputStream is) {
        in = new DataInputStream(is);
    }

    public final void run() {
        for (int i = 0; i < 10; i++) {
            try {
                double x = in.readDouble();
                logger.info("Consumer" + x);
            } catch (IOException e) {
                logger.info("Error: " + e);
            }
        }
    }


}
