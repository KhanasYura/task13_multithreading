package com.khanas.part1.model;

import java.io.*;

public class Filter extends Thread {
    private DataInputStream in;
    private DataOutputStream out;
    private double total = 0;
    private int count = 0;

    public Filter(InputStream is, OutputStream os) {
        in = new DataInputStream(is);
        out = new DataOutputStream(os);
    }

    public final void run() {
        for (int i = 0; i < 10; i++) {
            try {
                double x = in.readDouble();
                total += x;
                count++;
                if (count != 0) {
                    System.out.println("Filter:" + total / count);
                    out.writeDouble(total / count);
                }
            } catch (IOException e) {
                System.out.println("Error: " + e);
            }
        }
    }

}
