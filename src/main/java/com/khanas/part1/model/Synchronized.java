package com.khanas.part1.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Synchronized {

    Logger logger = LogManager.getLogger();
    private int a;
    private final Object monitor = new Object();

    public Synchronized() {
        this.a = 0;
    }

    public final int getA() {
        return a;
    }

    public final void myMethod1() {
        synchronized (monitor) {
            for (int i = 0; i < 5; i++) {
                a++;
                logger.info("Hi I'm synchronized method(1)!");
            }
        }
    }

    public final void myMethod2() {
        synchronized (monitor) {
            for (int i = 0; i < 5; i++) {
                a++;
                logger.info("Hi I'm synchronized method(2)!");
            }
        }
    }

    public final void myMethod3() {
        synchronized (monitor) {
            for (int i = 0; i < 5; i++) {
                a++;
                logger.info("Hi I'm synchronized method(3)!");
            }
        }
    }

    public final void runMethods() {
        myMethod1();
        myMethod2();
        myMethod3();
    }
}
