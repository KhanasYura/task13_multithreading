package com.khanas.part1.model;

public class SynchronizedDifferent {
    private int a;
    private final Object monitor1 = new Object();
    private final Object monitor2 = new Object();
    private final Object monitor3 = new Object();

    public SynchronizedDifferent() {
        this.a = 0;
    }

    public final int getA() {
        return a;
    }

    public final void myMethod1() {
        synchronized (monitor1) {
            for (int i = 0; i < 5; i++) {
                a++;
                System.out.println("Hi I'm synchronized method(1)!");
            }
        }
    }

    public final void myMethod2() {
        synchronized (monitor2) {
            for (int i = 0; i < 5; i++) {
                a++;
                System.out.println("Hi I'm synchronized method(2)!");
            }
        }
    }

    public final void myMethod3() {
        synchronized (monitor3) {
            for (int i = 0; i < 5; i++) {
                a++;
                System.out.println("Hi I'm synchronized method(3)!");
            }
        }
    }

    public final void runMethods() {
        myMethod1();
        myMethod2();
        myMethod3();
    }
}
