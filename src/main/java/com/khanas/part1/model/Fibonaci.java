package com.khanas.part1.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Fibonaci implements Runnable {

    private Logger logger = LogManager.getLogger();
    private int n;

    public Fibonaci(final int n) {
        this.n = n;
    }

    public final void run() {
        long f0 = 0;
        long f1 = 1;
        for (int i = 0; i < n; i++) {
            long fn = f0 + f1;
            f0 = f1;
            f1 = fn;
            logger.info(fn);
        }
    }
}
