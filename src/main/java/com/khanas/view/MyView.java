package com.khanas.view;

import com.khanas.part1.model.*;
import com.khanas.part2.model.ConsumerQueue;
import com.khanas.part2.model.LockTask;
import com.khanas.part2.model.ProducerQueue;
import com.khanas.part2.model.ReadWriteLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.*;

import static java.lang.System.exit;
import static java.lang.Thread.MAX_PRIORITY;
import static java.util.concurrent.TimeUnit.SECONDS;

public class MyView {

    private static Logger logger = LogManager.getLogger();
    private static Scanner sc = new Scanner(System.in);

    public MyView() throws InterruptedException, ExecutionException {
        logger.info("----------------Part1-------------------");

        logger.info("--------------Ping - pong---------------");
        new PingPong();

        logger.info("Enter n for Fibonacci: ");
        int n = sc.nextInt();

        logger.info("------------Thread Runnable-------------");
        task1(n);

        logger.info("------------ExecutorService-------------");
        task2(n);

        logger.info("--------------Callable------------------");
        task3(n);

        logger.info("----------------Random------------------");
        task4();

        logger.info("----------------Pipes------------------");
        task5();

        logger.info("------------Synchronized---------------");
        task6();

        logger.info("-------Synchronized Different----------");
        task7();

        logger.info("---------------Part 2------------------");

        logger.info("----------------Lock-------------------");
        task8();

        logger.info("-----------BlockingQueue--------------");
        task9();

        logger.info("-----------ReadWriteLock--------------");
        task10();

        exit(0);
    }

    private final void task1(final int n) throws InterruptedException {
        Thread threadFibonacci1 = new Thread(new Fibonaci(n));
        Thread threadFibonacci2 = new Thread(new Fibonaci(n + 1));
        Thread threadFibonacci3 = new Thread(new Fibonaci(n + 2));
        threadFibonacci1.start();
        threadFibonacci2.start();
        threadFibonacci3.start();

        threadFibonacci1.join();
        threadFibonacci2.join();
        threadFibonacci3.join();
    }

    private final void task2(final int n) throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(2);
        Thread threadFibonacci4 = new Thread(new Fibonaci(n));
        Thread threadFibonacci5 = new Thread(new Fibonaci(n + 1));
        service.execute(threadFibonacci4);
        service.execute(threadFibonacci5);

        service.shutdown();
        service.awaitTermination(MAX_PRIORITY, TimeUnit.HOURS);
    }

    private final void task3(final int n) throws InterruptedException,
            ExecutionException {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        FibonaciCallable fc1 = new FibonaciCallable(n);
        FibonaciCallable fc2 = new FibonaciCallable(n + 1);
        Future<Integer> future1 = executor.submit(fc1);
        Future<Integer> future2 = executor.submit(fc2);

        logger.info("Sum of " + n + " fibonacci numbers: " + future1.get());
        logger.info("Sum of " + (n + 1) + " fibonacci numbers: "
                + future2.get());
    }

    private final void task4() throws InterruptedException {
        logger.info("Enter number of sheduled threads: ");
        int index = sc.nextInt();
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(index);

        for (int i = 0; i < index; i++) {
            Random random = new Random();
            int time = 1 + random.nextInt(10);
            ses.schedule(() -> logger.info(time + " sec"), time, SECONDS);
        }
        ses.shutdown();
        ses.awaitTermination(MAX_PRIORITY, TimeUnit.MINUTES);
    }

    private final void task5() throws InterruptedException {
        try {
            PipedOutputStream pout1 = new PipedOutputStream();
            PipedInputStream pin1 = new PipedInputStream(pout1);

            PipedOutputStream pout2 = new PipedOutputStream();
            PipedInputStream pin2 = new PipedInputStream(pout2);

            Producer prod = new Producer(pout1);
            Consumer cons = new Consumer(pin1);

            prod.start();
            cons.start();

            prod.join();
            cons.join();
        } catch (IOException e) {
            logger.info("Error");
        }
    }

    private final void task6() throws InterruptedException {
        Synchronized sync = new Synchronized();
        Thread thread1 = new Thread(sync::runMethods);
        Thread thread2 = new Thread(sync::runMethods);
        Thread thread3 = new Thread(sync::runMethods);

        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();

        logger.info(sync.getA());
    }

    private final void task7() throws InterruptedException {
        SynchronizedDifferent syncDiff = new SynchronizedDifferent();
        Thread thread1 = new Thread(syncDiff::runMethods);
        Thread thread2 = new Thread(syncDiff::runMethods);
        Thread thread3 = new Thread(syncDiff::runMethods);

        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();

        logger.info(syncDiff.getA());
    }

    private final void task8() throws InterruptedException {
        LockTask lock = new LockTask();
        Thread thread1 = new Thread(lock::runMethods);
        Thread thread2 = new Thread(lock::runMethods);
        Thread thread3 = new Thread(lock::runMethods);

        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();

        logger.info(lock.getA());
    }

    private final void task9() {
        BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(5);

        Thread thr1 = new Thread(new ProducerQueue(queue));
        Thread thr2 = new Thread(new ConsumerQueue(queue));

        thr1.start();
        thr2.start();

        try {
            thr1.join();
            thr2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private final void task10() throws InterruptedException {
        ReadWriteLock rwl = new ReadWriteLock();
        int a = 10;
        rwl.readLock();
        logger.info("Reading");
        logger.info(a);
        rwl.readUnlock();
        rwl.writeLock();
        logger.info("Writing");
        a = 20;
        rwl.writeUnlock();
        rwl.readLock();
        logger.info("Reading");
        logger.info(a);
        rwl.readUnlock();
    }
}
