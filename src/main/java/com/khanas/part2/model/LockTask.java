package com.khanas.part2.model;

import java.util.concurrent.locks.ReentrantLock;

public class LockTask {

    private int a;
    private final ReentrantLock lock = new ReentrantLock();

    public LockTask() {
        this.a = 0;
    }

    public final int getA() {
        return a;
    }

    public final void myMethod1() {
        lock.lock();
            for (int i = 0; i < 5; i++) {
                a++;
                System.out.println("Hi I'm synchronized method(1)!");
            }
        lock.unlock();
    }

    public final void myMethod2() {
        lock.lock();
            for (int i = 0; i < 5; i++) {
                a++;
                System.out.println("Hi I'm synchronized method(2)!");
            }
        lock.unlock();
    }

    public final void myMethod3() {
        lock.lock();
            for (int i = 0; i < 5; i++) {
                a++;
                System.out.println("Hi I'm synchronized method(3)!");
            }
        lock.unlock();
    }

    public final void runMethods() {
        myMethod1();
        myMethod2();
        myMethod3();
    }
}
