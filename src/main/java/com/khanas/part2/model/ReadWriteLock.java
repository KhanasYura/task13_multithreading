package com.khanas.part2.model;

public class ReadWriteLock {
    static final int WRITE_LOCKED = -1, FREE = 0;

    private int numberOfReaders = FREE;
    private Thread currentWriteLockOwner;

    public synchronized void readLock() throws InterruptedException {
        while (numberOfReaders == WRITE_LOCKED) {
            throw new IllegalMonitorStateException();
        }
        numberOfReaders++;
    }

    public synchronized void readUnlock() {
        if (numberOfReaders <= 0) {
            throw new IllegalMonitorStateException();
        }
        numberOfReaders--;
        if (numberOfReaders == FREE) {
            notifyAll();
        }
    }

    public synchronized void writeLock() throws InterruptedException {
        while (numberOfReaders != FREE) {
            throw new IllegalMonitorStateException();
        }
        numberOfReaders = WRITE_LOCKED;
        currentWriteLockOwner = Thread.currentThread();
    }

    public synchronized void writeUnlock() {
        if (numberOfReaders != WRITE_LOCKED || currentWriteLockOwner != Thread.currentThread()) {
            throw new IllegalMonitorStateException();
        }
        numberOfReaders = FREE;
        currentWriteLockOwner = null;
        notifyAll();
    }
}
