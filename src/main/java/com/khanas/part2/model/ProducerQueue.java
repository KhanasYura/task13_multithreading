package com.khanas.part2.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;

public class ProducerQueue implements Runnable{

    Logger logger = LogManager.getLogger();
    private final BlockingQueue<Integer> queue;

    public ProducerQueue(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            for (int i = 0; i < 10; i++) {
                logger.info("Put: " + i);
                queue.put(i);
                Thread.sleep(200);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

    }

}

