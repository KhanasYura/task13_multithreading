package com.khanas.part2.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.BlockingQueue;

public class ConsumerQueue implements Runnable {

    Logger logger = LogManager.getLogger();
    private final BlockingQueue<Integer> queue;

    public ConsumerQueue(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {

        try {
            for (int i = 0; i < 10; i++) {
                Integer take = queue.take();
                logger.info("Take: " + take);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

    }

}
